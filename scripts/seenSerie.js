/* eslint-disable */

const { MongoClient } = require('mongodb');
require('dotenv').config({ path: '.env.development.local' });

const databaseInformation = getDatabaseInformation();
let uri;
if (process.env.ENV === 'dev') {
  uri = `mongodb://${databaseInformation.username}:${databaseInformation.password}@${databaseInformation.host}:${databaseInformation.port}/${databaseInformation.database}`;
} else {
  uri = `mongodb+srv://${databaseInformation.username}:${databaseInformation.password}@${databaseInformation.host}/${databaseInformation.database}`;
}

console.log(uri);
const client = new MongoClient(uri);

async function run() {
  try {
    await client.connect();
    const database = client.db(process.env.DATABASE);
    const collection = database.collection('media');

    collection.updateMany({ type: 'serie', hasSeen: true }, { $set: { hasSeen: false } }).then(
      () => console.log(`[${new Date().toDateString()}] Set hasSeen to false for all serie`),
      (err) => console.error('Fail tp reset hasSeen for all serie', err)
    );
  } finally {
    await client.close();
  }
}
run().catch(console.dir);

function getDatabaseInformation() {
  return {
    host: process.env.DB_ADDRESS,
    port: +process.env.DB_PORT,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DATABASE,
  };
}

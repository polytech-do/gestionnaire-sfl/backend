import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigService } from 'src/app/common/config/config.service';
import { ConfigModule } from 'src/app/common/config/config.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.getDefaultDbConfig(),
      }),
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}

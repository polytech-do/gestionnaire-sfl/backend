import { ApiProperty } from '@nestjs/swagger';
import { User } from 'src/app/user/schema/user.schema';

export class CreateAuthorDto {
  @ApiProperty({
    description: 'lastName of the author',
    example: 'Martin',
  })
  lastName: string;

  @ApiProperty({
    description: 'firstname of the author',
    example: 'Clara',
  })
  firstname: string;

  @ApiProperty({
    description: 'owner of the author',
    type: User,
  })
  owner: User;
}

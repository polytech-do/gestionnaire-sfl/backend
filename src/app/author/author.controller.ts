import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Req } from '@nestjs/common';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { JwtAuthGuard } from '../auth/jwt.guard';
import { JwtRequest } from '../auth/jwt/jwt-request.type';
import { IdValidationPipe } from '../common/pipes/id-validation.pipe';
import { Endpoint } from '../endpoint';
import { AuthorService } from './author.service';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';
import { Author } from './schema/author.schema';

@ApiTags(Endpoint.AUTHOR)
@ApiUnauthorizedResponse({ description: 'You need to be conected to access to this ressource' })
@ApiNotFoundResponse({ description: 'The ressource was not found' })
@UseGuards(JwtAuthGuard)
@Controller(Endpoint.AUTHOR)
export class AuthorController {
  constructor(private readonly authorService: AuthorService) {}

  @ApiBody({ type: CreateAuthorDto })
  @ApiCreatedResponse({ description: 'The author has been successfully created', type: Author })
  @Post()
  create(@Req() req: JwtRequest, @Body() createAuthorDto: CreateAuthorDto): Observable<Author> {
    return this.authorService.create(createAuthorDto, req.user);
  }

  @ApiOkResponse({ description: 'All author has been successfully returned', type: [Author] })
  @Get()
  findAll(@Req() req: JwtRequest): Observable<Author[]> {
    return this.authorService.findAll(req.user);
  }

  @ApiOkResponse({ description: 'The count of author has been successfully returned' })
  @Get('count')
  count(@Req() req: JwtRequest): Observable<number> {
    return this.authorService.count(req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the author', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The author has been successfully returned', type: Author })
  @Get(':id')
  findOne(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<Author> {
    return this.authorService.findOne(id, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the author', example: '5ff35a19056faa5b90bdae17' })
  @ApiBody({ type: UpdateAuthorDto })
  @ApiOkResponse({ description: 'The author has been successfully updated' })
  @Put(':id')
  update(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string, @Body() updateAuthorDto: UpdateAuthorDto): Observable<void> {
    return this.authorService.update(id, updateAuthorDto, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the author', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The author has been successfully deleted' })
  @Delete(':id')
  remove(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<void> {
    return this.authorService.remove(id, req.user);
  }
}

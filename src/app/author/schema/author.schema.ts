import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { User } from 'src/app/user/schema/user.schema';

export type AuthorDocument = Author & Document;

@Schema()
export class Author {
  @ApiProperty({
    description: 'id of the author',
    example: '5ff35a19056faa5b90bdae17',
  })
  _id: string;

  @ApiProperty({
    description: 'lastName of the author',
    example: 'Martin',
  })
  @Prop()
  lastName: string;

  @ApiProperty({
    description: 'firstname of the author',
    example: 'Clara',
  })
  @Prop()
  firstname: string;

  @ApiProperty({
    description: 'owner of the author',
    type: User,
  })
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  owner: User;
}

export const AuthorSchema = SchemaFactory.createForClass(Author);

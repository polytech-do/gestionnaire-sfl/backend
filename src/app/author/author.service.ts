import { forwardRef, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Media, MediaDocument } from '../media/schema/media.schema';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { User } from '../user/schema/user.schema';
import { UserService } from '../user/user.service';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';
import { Author, AuthorDocument } from './schema/author.schema';

@Injectable()
export class AuthorService {
  constructor(
    @InjectModel(Author.name) private authorModel: Model<AuthorDocument>,
    @InjectModel(Media.name) private mediaModel: Model<MediaDocument>,
    @Inject(forwardRef(() => UserService))
    private userService: UserService
  ) {}

  create(createAuthorDto: CreateAuthorDto, me: CreateUserDto): Observable<Author> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          const createAuthor: CreateAuthorDto = { ...createAuthorDto, owner: user };
          const createdAuthor: AuthorDocument = new this.authorModel(createAuthor);
          return createdAuthor.save();
        }
      })
    );
  }

  findAll(me: CreateUserDto): Observable<Author[]> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.authorModel.find({ owner: user }).populate({ path: 'owner', select: '-token', model: 'User' }).exec();
        }
      })
    );
  }

  findOne(id: string, me: CreateUserDto): Observable<Author> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return from(
            this.authorModel.findOne({ _id: id, owner: user }).populate({ path: 'owner', select: '-token', model: 'User' }).exec()
          ).pipe(
            tap((author: Author) => {
              if (!author) {
                throw new NotFoundException();
              }
            })
          );
        }
      })
    );
  }

  update(id: string, updateAuthorDto: UpdateAuthorDto, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.authorModel.updateOne({ _id: id, owner: user }, updateAuthorDto, { new: true });
        }
      }),
      map(() => {
        return;
      })
    );
  }

  remove(id: string, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return from(this.findOne(id, me)).pipe(
            switchMap((author: Author) => {
              return from(this.mediaModel.updateMany({ authors: author }, { $pullAll: { authors: [author] } })).pipe(
                switchMap(() => this.authorModel.deleteOne({ _id: id, owner: user }))
              );
            })
          );
        }
      }),
      map(() => {
        return;
      })
    );
  }

  removeByOwner(owner: User): Observable<void> {
    return from(this.authorModel.deleteMany({ owner: owner })).pipe(
      map(() => {
        return;
      })
    );
  }

  count(me: CreateUserDto): Observable<number> | any {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.authorModel.countDocuments({ owner: user });
        }
      })
    );
  }
}

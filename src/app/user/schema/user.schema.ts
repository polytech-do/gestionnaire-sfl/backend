import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @ApiProperty({
    description: 'id of the user',
    example: '5ff35a19056faa5b90bdae17',
    type: String,
  })
  _id: string;

  @ApiProperty({
    description: 'avatar of the user',
    type: String,
  })
  @Prop()
  avatar?: string;

  @ApiProperty({
    description: 'avatar of the user',
    format: 'email',
    example: 'noel@gmail.com',
    type: String,
  })
  @Prop()
  email: string;

  @ApiProperty({
    description: 'displayName of the user',
    example: 'Charle Martin',
    type: String,
  })
  @Prop()
  displayName: string;

  @ApiProperty({
    description: 'token of the authenticated user',
    type: String,
  })
  @Prop()
  token?: string;
}

export const UserSchema = SchemaFactory.createForClass(User);

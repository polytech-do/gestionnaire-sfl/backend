import { Controller, Delete, Param, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { Endpoint } from '../endpoint';
import { ApiOkResponse, ApiParam, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwt.guard';
import { IdValidationPipe } from '../common/pipes/id-validation.pipe';
import { Observable } from 'rxjs';

@ApiTags(Endpoint.USER)
@ApiUnauthorizedResponse({ description: 'You need to be conected to access to this ressource' })
@UseGuards(JwtAuthGuard)
@Controller(Endpoint.USER)
export class UserController {
  constructor(private readonly userService: UserService) {}

  // @ApiBody({ type: CreateUserDto })
  // @ApiCreatedResponse({ description: 'The user has been successfully created', type: User })
  // @Post()
  // create(@Body() createUserDto: CreateUserDto): Observable<User> {
  //   return this.userService.create(createUserDto);
  // }

  // @ApiOkResponse({ description: 'All user has been successfully returned', type: [User] })
  // @Get()
  // findAll(): Observable<User[]> {
  //   return this.userService.findAll();
  // }

  // @ApiParam({ name: 'id', type: String, description: 'id of the user', example: '5ff35a19056faa5b90bdae17' })
  // @ApiOkResponse({ description: 'The user has been successfully returned', type: User })
  // @Get(':id')
  // findOne(@Param('id', IdValidationPipe) id: string): Observable<User> {
  //   return this.userService.findOne(id);
  // }

  // @ApiParam({ name: 'id', type: String, description: 'id of the user', example: '5ff35a19056faa5b90bdae17' })
  // @ApiBody({ type: UpdateUserDto })
  // @ApiOkResponse({ description: 'The user has been successfully updated' })
  // @Put(':id')
  // update(@Param('id', IdValidationPipe) id: string, @Body() updateUserDto: UpdateUserDto): Observable<void> {
  //   return this.userService.update(id, updateUserDto);
  // }

  @ApiParam({ name: 'id', type: String, description: 'id of the user', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The user has been successfully deleted' })
  @Delete(':id')
  remove(@Param('id', IdValidationPipe) id: string): Observable<void> {
    return this.userService.remove(id);
  }
}

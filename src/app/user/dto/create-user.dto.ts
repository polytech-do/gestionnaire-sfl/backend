import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({
    description: 'avatar of the user',
  })
  avatar?: string;

  @ApiProperty({
    description: 'avatar of the user',
    format: 'email',
    example: 'noel@gmail.com',
  })
  email: string;

  @ApiProperty({
    description: 'displayname of the user',
    example: 'Charle Martin',
  })
  displayName: string;

  @ApiProperty({
    description: 'token of the authenticated user',
  })
  token?: string;
}

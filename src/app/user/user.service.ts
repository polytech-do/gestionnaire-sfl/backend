import { forwardRef, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { forkJoin, from, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { AuthorService } from '../author/author.service';
import { GenreService } from '../genre/genre.service';
import { Media, MediaDocument } from '../media/schema/media.schema';
import { SharedLinkService } from '../shared-link/shared-link.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User, UserDocument } from './schema/user.schema';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(Media.name) private mediaModel: Model<MediaDocument>,
    @Inject(forwardRef(() => GenreService))
    private genreService: GenreService,
    @Inject(forwardRef(() => AuthorService))
    private authorService: AuthorService,
    @Inject(forwardRef(() => SharedLinkService))
    private sharedLinkService: SharedLinkService
  ) {}

  create(createUserDto: CreateUserDto): Observable<User> {
    const userUser: UserDocument = new this.userModel(createUserDto);
    return from(userUser.save());
  }

  findAll(): Observable<User[]> {
    return from(this.userModel.find());
  }

  findOne(id: string): Observable<User> {
    return from(this.userModel.findById(id)).pipe(
      tap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        }
      })
    );
  }

  update(id: string, updateUserDto: UpdateUserDto): Observable<void> {
    return from(this.userModel.updateOne({ _id: id }, updateUserDto, { new: true })).pipe(
      map(() => {
        return;
      })
    );
  }

  remove(id: string): Observable<void> {
    return this.findOne(id).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return forkJoin([
            this.mediaModel.deleteMany({ owner: user }),
            this.authorService.removeByOwner(user),
            this.genreService.removeByOwner(user),
            this.sharedLinkService.removeByOwner(user),
            this.userModel.deleteOne({ _id: id }),
          ]);
        }
      }),
      map(() => {
        return;
      })
    );
  }

  findOneByEmail(email: string): Observable<User> {
    return from(this.userModel.findOne({ email: email }));
  }

  findOneByToken(token: string): Observable<User> {
    return from(this.userModel.findOne({ token: token })).pipe(
      map((entity: User) => {
        if (entity === null) {
          throw new NotFoundException();
        }
        return entity;
      })
    );
  }

  updateToken(userEmail: string, token: string): Observable<void> {
    return from(this.userModel.updateOne({ email: userEmail }, { token }));
  }

  createOrUpdateUser(oAuthUser: CreateUserDto): Observable<User> {
    return from(this.findOneByEmail(oAuthUser.email)).pipe(
      switchMap((user: User) => {
        // If user found => update
        if (user) {
          user.avatar = oAuthUser.avatar;
          user.displayName = oAuthUser.displayName;
          user.email = oAuthUser.email;
          user.token = oAuthUser.token;
          return this.userModel.updateOne({ email: oAuthUser.email }, user);
        }
        // Else => create
        return this.create({
          ...oAuthUser,
          token: oAuthUser.token,
        });
      }),
      // Get the updated / new user
      switchMap(() => this.findOneByEmail(oAuthUser.email))
    );
  }
}

import { forwardRef, Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './schema/user.schema';
import { Media, MediaSchema } from '../media/schema/media.schema';
import { GenreModule } from '../genre/genre.module';
import { AuthorModule } from '../author/author.module';
import { SharedLinkModule } from '../shared-link/shared-link.module';

@Module({
  imports: [
    forwardRef(() => GenreModule),
    forwardRef(() => AuthorModule),
    forwardRef(() => SharedLinkModule),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    MongooseModule.forFeature([{ name: Media.name, schema: MediaSchema, collection: 'media' }]),
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}

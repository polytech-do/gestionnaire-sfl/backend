import { Controller, Get, Post, Body, Put, Param, Delete, Query, UseGuards, Req } from '@nestjs/common';
import { Endpoint } from '../../endpoint';
import { BookService } from './book.service';
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiParam,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Book } from './shema/book.shema';
import { Observable } from 'rxjs';
import { MediaQuerryParams } from 'src/app/common/model/media-querry-params.type';
import { JwtAuthGuard } from 'src/app/auth/jwt.guard';
import { JwtRequest } from 'src/app/auth/jwt/jwt-request.type';
import { IdValidationPipe } from 'src/app/common/pipes/id-validation.pipe';

@ApiTags(Endpoint.BOOK)
@ApiUnauthorizedResponse({ description: 'You need to be conected to access to this ressource' })
@ApiNotFoundResponse({ description: 'The ressource was not found' })
@UseGuards(JwtAuthGuard)
@Controller(Endpoint.BOOK)
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @ApiBody({ type: CreateBookDto })
  @ApiCreatedResponse({ description: 'The book has been successfully created', type: Book })
  @Post()
  create(@Req() req: JwtRequest, @Body() createBookDto: CreateBookDto): Observable<Book> {
    return this.bookService.create(createBookDto, req.user);
  }

  @ApiOkResponse({ description: 'All book has been successfully returned', type: [Book] })
  @Get()
  @ApiQuery({ type: MediaQuerryParams })
  findAll(@Req() req: JwtRequest, @Query() queries: MediaQuerryParams): Observable<Book[]> {
    return this.bookService.findAll(req.user, queries);
  }

  @ApiOkResponse({ description: 'The count of book has been successfully returned' })
  @Get('count')
  count(@Req() req: JwtRequest): Observable<number> {
    return this.bookService.count(req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the book', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The book has been successfully returned', type: Book })
  @Get(':id')
  findOne(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<Book> {
    return this.bookService.findOne(id, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the book', example: '5ff35a19056faa5b90bdae17' })
  @ApiBody({ type: UpdateBookDto })
  @ApiOkResponse({ description: 'The book has been successfully updated' })
  @Put(':id')
  update(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string, @Body() updateBookDto: UpdateBookDto): Observable<void> {
    return this.bookService.update(id, updateBookDto, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the book', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The book has been successfully deleted' })
  @Delete(':id')
  remove(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<void> {
    return this.bookService.remove(id, req.user);
  }
}

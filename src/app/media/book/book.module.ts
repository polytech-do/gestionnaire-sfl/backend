import { forwardRef, Module } from '@nestjs/common';
import { BookService } from './book.service';
import { BookController } from './book.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Book, BookSchema } from './shema/book.shema';
import { BookFilters } from './book.filters';
import { UserModule } from 'src/app/user/user.module';

@Module({
  imports: [forwardRef(() => UserModule), MongooseModule.forFeature([{ name: Book.name, schema: BookSchema, collection: 'media' }])],
  controllers: [BookController],
  providers: [BookService, BookFilters],
})
export class BookModule {}

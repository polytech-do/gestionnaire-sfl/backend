import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { Genre } from 'src/app/genre/schema/genre.schema';
import * as mongoose from 'mongoose';
import { Author } from 'src/app/author/schema/author.schema';
import { User } from 'src/app/user/schema/user.schema';

export type BookDocument = Book & Document;

@Schema()
export class Book {
  @ApiProperty({
    description: 'id of the book',
    example: '5ff35a19056faa5b90bdae17',
  })
  _id: string;

  @ApiProperty({
    description: 'title of the book',
    example: 'Le rapport de brodeck',
  })
  @Prop()
  title: string;

  @ApiProperty({
    description: 'description of the book',
    example: 'Le personnage principal, Brodeck, revient dans son village après avoir été déporté dans un camp',
  })
  @Prop()
  description?: string;

  @ApiProperty({
    description: 'url of the book image',
    example: 'https://www.ligneclaire.info/wp-content/uploads/2016/06/BD-Planche-Brodeck.jpg',
  })
  @Prop()
  image?: string;

  @ApiProperty({
    description: 'rate of the book',
    example: 5,
    maximum: 5,
  })
  @Prop({ max: 5 })
  rate?: number;

  @ApiProperty({
    description: 'list of genres of the book',
    type: [Genre],
  })
  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: Genre.name }] })
  genres?: Genre[];

  @ApiProperty({
    description: 'authors of the book',
    type: [Author],
  })
  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: Author.name }] })
  authors?: Author[];

  @ApiProperty({
    description: 'owner of the book',
    type: User,
  })
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  owner: User;

  @ApiProperty({
    description: "if true the book is one of the user's favourites",
    example: true,
  })
  @Prop()
  love: boolean;

  @ApiProperty({
    description: 'if true the book is in the wish list of the user',
    example: false,
  })
  @Prop()
  wish: boolean;

  @ApiProperty({
    description: 'if true the user watch the book',
    example: true,
  })
  @Prop()
  inProgress: boolean;

  @ApiProperty({
    description: 'media type',
    example: 'book',
  })
  @Prop()
  type: string;
}

export const BookSchema = SchemaFactory.createForClass(Book);

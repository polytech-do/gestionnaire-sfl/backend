import { ApiProperty } from '@nestjs/swagger';
import { Author } from 'src/app/author/schema/author.schema';
import { Genre } from 'src/app/genre/schema/genre.schema';
import { User } from 'src/app/user/schema/user.schema';

export class CreateBookDto {
  @ApiProperty({
    description: 'title of the book',
    example: 'Le rapport de brodeck',
  })
  title: string;

  @ApiProperty({
    description: 'description of the book',
    example: 'Le personnage principal, Brodeck, revient dans son village après avoir été déporté dans un camp',
  })
  description?: string;

  @ApiProperty({
    description: 'url of the book image',
    example: 'https://www.ligneclaire.info/wp-content/uploads/2016/06/BD-Planche-Brodeck.jpg',
  })
  image?: string;

  @ApiProperty({
    description: 'rate of the book',
    example: 5,
  })
  rate?: number;

  @ApiProperty({
    description: 'list of genres of the book',
    type: [Genre],
  })
  genres?: Genre[];

  @ApiProperty({
    description: 'authors of the book',
    type: [Author],
  })
  authors?: Author[];

  @ApiProperty({
    description: 'owner of the film',
    type: User,
  })
  owner?: User;

  @ApiProperty({
    description: "if true the book is one of the user's favourites",
    example: true,
  })
  love: boolean;

  @ApiProperty({
    description: 'if true the book is in the wish list of the user',
    example: false,
  })
  wish: boolean;

  @ApiProperty({
    description: 'if true the user watch the book',
    example: true,
  })
  inProgress: boolean;

  @ApiProperty({
    description: 'media type',
    example: 'book',
  })
  type: string;
}

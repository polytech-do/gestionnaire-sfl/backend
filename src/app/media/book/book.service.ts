import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Author } from 'src/app/author/schema/author.schema';
import { MediaQuerryParams } from 'src/app/common/model/media-querry-params.type';
import { TypeEnum } from 'src/app/common/model/type-enum';
import { Genre } from 'src/app/genre/schema/genre.schema';
import { CreateUserDto } from 'src/app/user/dto/create-user.dto';
import { User } from 'src/app/user/schema/user.schema';
import { UserService } from 'src/app/user/user.service';
import { CreateFilmDto } from '../film/dto/create-film.dto';
import { FilmDocument } from '../film/shema/film.shema';
import { BookFilters } from './book.filters';
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';
import { Book, BookDocument } from './shema/book.shema';

@Injectable()
export class BookService {
  constructor(
    @InjectModel(Book.name) private bookModel: Model<BookDocument>,
    private bookFilters: BookFilters,
    private userService: UserService
  ) {}

  create(createBookDto: CreateBookDto, me: CreateUserDto): Observable<Book> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          const createFilm: CreateFilmDto = { ...createBookDto, owner: user };
          const createdFilm: FilmDocument = new this.bookModel(createFilm);
          return createdFilm.save();
        }
      })
    );
  }

  findAll(me: CreateUserDto, queries?: MediaQuerryParams): Observable<Book[]> {
    return this.bookFilters.buildFilters(queries, me).pipe(
      switchMap((filters: MediaQuerryParams) => {
        return this.bookModel
          .find(filters)
          .populate({ path: 'genres', model: Genre.name })
          .populate({ path: 'authors', model: Author.name })
          .populate({ path: 'owner', select: '-token', model: User.name })
          .exec();
      })
    );
  }

  findOne(id: string, me: CreateUserDto): Observable<Book> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return from(
            this.bookModel
              .findOne({ _id: id, owner: user })
              .populate({ path: 'genres', model: Genre.name })
              .populate({ path: 'authors', model: Author.name })
              .populate({ path: 'owner', select: '-token', model: User.name })
              .exec()
          ).pipe(
            tap((book: Book) => {
              if (!book) {
                throw new NotFoundException();
              }
            })
          );
        }
      })
    );
  }

  update(id: string, updateBookDto: UpdateBookDto, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.bookModel.updateOne({ _id: id, owner: user }, updateBookDto, { new: true });
        }
      }),
      map(() => {
        return;
      })
    );
  }

  remove(id: string, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.bookModel.deleteOne({ _id: id, owner: user });
        }
      }),
      map(() => {
        return;
      })
    );
  }

  count(me: CreateUserDto): Observable<number> | any {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.bookModel.countDocuments({ type: TypeEnum.book, owner: user });
        }
      })
    );
  }
}

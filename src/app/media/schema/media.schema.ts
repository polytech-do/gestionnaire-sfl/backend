import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Serie } from '../serie/schema/serie.schema';
import * as mongoose from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Genre } from 'src/app/genre/schema/genre.schema';
import { Author } from 'src/app/author/schema/author.schema';
import { User } from 'src/app/user/schema/user.schema';

export type MediaDocument = Media & Document;

@Schema()
export class Media {
  @ApiProperty({
    description: 'list of genres of the media',
    type: [Genre],
  })
  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: Genre.name }] })
  genres?: Genre[];

  @ApiProperty({
    description: 'authors of the media',
    type: [Author],
  })
  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: Author.name }] })
  authors?: Author[];

  @ApiProperty({
    description: 'owner of the media',
    type: User,
  })
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  owner: User;
}

export const MediaSchema = SchemaFactory.createForClass(Serie);

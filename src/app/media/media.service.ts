import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { Author } from 'src/app/author/schema/author.schema';
import { Genre } from 'src/app/genre/schema/genre.schema';
import { User } from 'src/app/user/schema/user.schema';
import { Media, MediaDocument } from './schema/media.schema';

@Injectable()
export class MediaService {
  constructor(@InjectModel(Media.name) private serieModel: Model<MediaDocument>) {}

  findAll(me: User): Observable<Media[]> {
    return from(
      this.serieModel
        .find({ owner: me })
        .populate({ path: 'genres', model: Genre.name })
        .populate({ path: 'authors', model: Author.name })
        .populate({ path: 'owner', select: '-token', model: User.name })
        .exec()
    );
  }
}

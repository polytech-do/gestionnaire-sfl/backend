import { ApiProperty } from '@nestjs/swagger';
import { Author } from 'src/app/author/schema/author.schema';
import { Genre } from 'src/app/genre/schema/genre.schema';
import { User } from 'src/app/user/schema/user.schema';
import { WeekDay } from '../type/weekday.enum';

export class CreateSerieDto {
  @ApiProperty({
    description: 'title of the serie',
    example: 'Shingeki no Kyojin',
  })
  title: string;

  @ApiProperty({
    description: 'description of the serie',
    example:
      "Plus de cent ans avant le début de l'histoire, des créatures géantes humanoïdes nommées Titans sont subitement apparues et ont presque anéanti l'humanité.",
  })
  description?: string;

  @ApiProperty({
    description: 'url of the serie image',
    example: 'https://upload.wikimedia.org/wikipedia/fr/9/94/Attaque_des_Titans_CMJN.svg',
  })
  image?: string;

  @ApiProperty({
    description: 'rate of the serie',
    example: 5,
  })
  rate?: number;

  @ApiProperty({
    description: 'list of genres of the serie',
    type: [Genre],
  })
  genres?: Genre[];

  @ApiProperty({
    description: 'authors of the serie',
    type: [Author],
  })
  authors?: Author[];

  @ApiProperty({
    description: 'owner of the serie',
    type: User,
  })
  owner?: User;

  @ApiProperty({
    description: "if true the serie is one of the user's favourites",
    example: true,
  })
  love: boolean;

  @ApiProperty({
    description: 'if true the serie is in the wish list of the user',
    example: false,
  })
  wish: boolean;

  @ApiProperty({
    description: 'if true the user watch the serie',
    example: true,
  })
  inProgress: boolean;

  @ApiProperty({
    description: 'starte date of the serie',
    example: '2013-07-02T22:00:00.000Z',
  })
  startDate?: Date;

  @ApiProperty({
    description: 'end date of the serie',
    example: '2013-07-02T22:00:00.000Z',
  })
  endDate?: Date;

  @ApiProperty({
    description: 'media type',
    example: 'serie',
  })
  type: string;

  @ApiProperty({
    description: 'The number of the last episode seen',
    example: 2,
  })
  lasEpisodeSeen?: number;

  @ApiProperty({
    description: 'releaseDay of the serie',
    example: '[4, 6]',
  })
  releaseDays?: WeekDay[];
}

import { Injectable, NotFoundException } from '@nestjs/common';
import { has, isArray } from 'lodash';
import { TypeEnum } from 'src/app/common/model/type-enum';
import { User } from 'src/app/user/schema/user.schema';
import { UserService } from 'src/app/user/user.service';
import { CreateUserDto } from 'src/app/user/dto/create-user.dto';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SerieQuerryParams } from './type/serieQuerryParams.type';
import { WeekDay } from './type/weekday.enum';

@Injectable()
export class SerieFilters {
  constructor(private userService: UserService) {}

  buildFilters(querry: SerieQuerryParams, me: CreateUserDto): Observable<SerieQuerryParams & { type: TypeEnum.serie }> {
    return this.userService.findOneByEmail(me.email).pipe(
      map((user: User) => {
        if (!user) {
          throw new NotFoundException();
        }
        const filter: SerieQuerryParams & { type: TypeEnum.serie } = { type: TypeEnum.serie, owner: user };
        if (has(querry, 'inProgress')) {
          filter.inProgress = querry.inProgress;
        }
        if (has(querry, 'title')) {
          filter.title = querry.title;
        }
        if (has(querry, 'wish')) {
          filter.wish = querry.wish;
        }
        if (has(querry, 'love')) {
          filter.love = querry.love;
        }
        if (has(querry, 'releaseDays')) {
          if (isArray(querry)) {
            filter.releaseDays = { $in: (querry.releaseDays as WeekDay[]).map((d) => +d) };
          } else {
            filter.releaseDays = { $in: [+querry.releaseDays] as WeekDay[] };
          }
        }
        if (has(querry, 'hasSeen')) {
          filter.hasSeen = querry.hasSeen;
        }
        return filter;
      })
    );
  }
}

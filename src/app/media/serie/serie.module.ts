import { Module } from '@nestjs/common';
import { SerieService } from './serie.service';
import { SerieController } from './serie.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Serie, SerieSchema } from './schema/serie.schema';
import { SerieFilters } from './serie.filters';
import { UserModule } from 'src/app/user/user.module';

@Module({
  imports: [UserModule, MongooseModule.forFeature([{ name: Serie.name, schema: SerieSchema, collection: 'media' }])],
  controllers: [SerieController],
  providers: [SerieService, SerieFilters],
})
export class SerieModule {}

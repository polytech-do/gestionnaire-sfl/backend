import { Controller, Get, Post, Body, Put, Param, Delete, Query, UseGuards, Req } from '@nestjs/common';
import { SerieService } from './serie.service';
import { CreateSerieDto } from './dto/create-serie.dto';
import { UpdateSerieDto } from './dto/update-serie.dto';
import { Endpoint } from '../../endpoint';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiParam,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { Serie } from './schema/serie.schema';
import { JwtAuthGuard } from 'src/app/auth/jwt.guard';
import { JwtRequest } from 'src/app/auth/jwt/jwt-request.type';
import { SerieQuerryParams } from './type/serieQuerryParams.type';
import { IdValidationPipe } from 'src/app/common/pipes/id-validation.pipe';

@ApiTags(Endpoint.SERIE)
@ApiUnauthorizedResponse({ description: 'You need to be conected to access to this ressource' })
@ApiNotFoundResponse({ description: 'The ressource was not found' })
@UseGuards(JwtAuthGuard)
@Controller(Endpoint.SERIE)
export class SerieController {
  constructor(private readonly serieService: SerieService) {}

  @ApiBody({ type: CreateSerieDto })
  @ApiCreatedResponse({ description: 'The serie has been successfully created', type: Serie })
  @Post()
  create(@Req() req: JwtRequest, @Body() createSerieDto: CreateSerieDto): Observable<Serie> {
    return this.serieService.create(createSerieDto, req.user);
  }

  @ApiOkResponse({ description: 'All series has been successfully returned', type: [Serie] })
  @ApiQuery({ type: SerieQuerryParams })
  @Get()
  findAll(@Req() req: JwtRequest, @Query() queries: SerieQuerryParams): Observable<Serie[]> {
    return this.serieService.findAll(req.user, queries);
  }

  @ApiOkResponse({ description: 'The count of serie has been successfully returned' })
  @Get('count')
  count(@Req() req: JwtRequest): Observable<number> {
    return this.serieService.count(req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the serie', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The serie has been successfully returned', type: Serie })
  @Get(':id')
  findOne(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<Serie> {
    return this.serieService.findOne(id, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the serie', example: '5ff35a19056faa5b90bdae17' })
  @ApiBody({ type: UpdateSerieDto })
  @ApiOkResponse({ description: 'The serie has been successfully updated' })
  @Put(':id')
  update(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string, @Body() updateSerieDto: UpdateSerieDto): Observable<void> {
    return this.serieService.update(id, updateSerieDto, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the serie', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The serie has been successfully deleted' })
  @Delete(':id')
  remove(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<void> {
    return this.serieService.remove(id, req.user);
  }
}

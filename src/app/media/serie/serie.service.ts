import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Author } from 'src/app/author/schema/author.schema';
import { TypeEnum } from 'src/app/common/model/type-enum';
import { Genre } from 'src/app/genre/schema/genre.schema';
import { CreateUserDto } from 'src/app/user/dto/create-user.dto';
import { User } from 'src/app/user/schema/user.schema';
import { UserService } from 'src/app/user/user.service';
import { CreateSerieDto } from './dto/create-serie.dto';
import { UpdateSerieDto } from './dto/update-serie.dto';
import { Serie, SerieDocument } from './schema/serie.schema';
import { SerieFilters } from './serie.filters';
import { SerieQuerryParams } from './type/serieQuerryParams.type';

@Injectable()
export class SerieService {
  private readonly logger = new Logger(SerieService.name);

  constructor(
    @InjectModel(Serie.name) private serieModel: Model<SerieDocument>,
    private serieFilters: SerieFilters,
    private userService: UserService
  ) {}

  create(createSerieDto: CreateSerieDto, me: CreateUserDto): Observable<Serie> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          const createSerie: CreateSerieDto = { ...createSerieDto, owner: user };
          const createdSerie: SerieDocument = new this.serieModel(createSerie);
          return createdSerie.save();
        }
      })
    );
  }

  findAll(me: CreateUserDto, queries?: SerieQuerryParams): Observable<Serie[]> {
    return this.serieFilters.buildFilters(queries, me).pipe(
      switchMap((filters: SerieQuerryParams) => {
        return this.serieModel
          .find(filters)
          .populate({ path: 'genres', model: Genre.name })
          .populate({ path: 'authors', model: Author.name })
          .populate({ path: 'owner', select: '-token', model: User.name })
          .exec();
      })
    );
  }

  findOne(id: string, me: CreateUserDto): Observable<Serie> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return from(
            this.serieModel
              .findOne({ _id: id, owner: user })
              .populate({ path: 'genres', model: Genre.name })
              .populate({ path: 'authors', model: Author.name })
              .populate({ path: 'owner', select: '-token', model: User.name })
              .exec()
          ).pipe(
            tap((serie: Serie) => {
              if (!serie) {
                throw new NotFoundException();
              }
            })
          );
        }
      })
    );
  }

  update(id: string, updateSerieDto: UpdateSerieDto, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.serieModel.updateOne({ _id: id, owner: user }, updateSerieDto, { new: true });
        }
      }),
      map(() => {
        return;
      })
    );
  }

  remove(id: string, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.serieModel.deleteOne({ _id: id, owner: user });
        }
      }),
      map(() => {
        return;
      })
    );
  }

  count(me: CreateUserDto): Observable<number> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.serieModel.countDocuments({ type: TypeEnum.serie, owner: user });
        }
      })
    );
  }

  @Cron(CronExpression.EVERY_DAY_AT_1AM, { timeZone: 'Europe/Paris' })
  resetHasSeen() {
    this.serieModel
      .updateMany({ type: TypeEnum.serie }, { $set: { hasSeen: false } })
      .exec()
      .then(
        () => this.logger.log('Set hasSeen to false for all serie'),
        (err) => this.logger.error('Fail tp reset hasSeen for all serie', err)
      );
  }
}

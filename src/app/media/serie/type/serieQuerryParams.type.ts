import { MediaQuerryParams } from 'src/app/common/model/media-querry-params.type';
import { WeekDay } from './weekday.enum';

export class SerieQuerryParams extends MediaQuerryParams {
  releaseDays?: WeekDay | WeekDay[] | { $in: WeekDay[] };
  hasSeen?: boolean;
}

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { Genre } from 'src/app/genre/schema/genre.schema';
import * as mongoose from 'mongoose';
import { Author } from 'src/app/author/schema/author.schema';
import { User } from 'src/app/user/schema/user.schema';
import { WeekDay } from '../type/weekday.enum';

export type SerieDocument = Serie & Document;

@Schema()
export class Serie {
  @ApiProperty({
    description: 'id of the serie',
    example: '5ff35a19056faa5b90bdae17',
  })
  _id: string;

  @ApiProperty({
    description: 'title of the serie',
    example: 'Shingeki no Kyojin',
  })
  @Prop()
  title: string;

  @ApiProperty({
    description: 'description of the serie',
    example:
      "Plus de cent ans avant le début de l'histoire, des créatures géantes humanoïdes nommées Titans sont subitement apparues et ont presque anéanti l'humanité.",
  })
  @Prop()
  description?: string;

  @ApiProperty({
    description: 'url of the serie image',
    example: 'https://upload.wikimedia.org/wikipedia/fr/9/94/Attaque_des_Titans_CMJN.svg',
  })
  @Prop()
  image?: string;

  @ApiProperty({
    description: 'rate of the serie',
    example: 5,
    maximum: 5,
  })
  @Prop({ max: 5 })
  rate?: number;

  @ApiProperty({
    description: 'list of genres of the serie',
    type: [Genre],
  })
  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: Genre.name }] })
  genres?: Genre[];

  @ApiProperty({
    description: 'authors of the serie',
    type: [Author],
  })
  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: Author.name }] })
  authors?: Author[];

  @ApiProperty({
    description: 'owner of the serie',
    type: User,
  })
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  owner: User;

  @ApiProperty({
    description: "if true the serie is one of the user's favourites",
    example: true,
  })
  @Prop()
  love: boolean;

  @ApiProperty({
    description: 'if true the serie is in the wish list of the user',
    example: false,
  })
  @Prop()
  wish: boolean;

  @ApiProperty({
    description: 'if true the user watch the serie',
    example: true,
  })
  @Prop()
  inProgress: boolean;

  @ApiProperty({
    description: 'starte date of the serie',
    example: '2013-07-02T22:00:00.000Z',
  })
  @Prop()
  startDate?: Date;

  @ApiProperty({
    description: 'end date of the serie',
    example: '2013-07-02T22:00:00.000Z',
  })
  @Prop()
  endDate?: Date;

  @ApiProperty({
    description: 'releaseDay of the serie',
    example: '[4, 6]',
  })
  @Prop()
  releaseDays?: WeekDay[];

  @ApiProperty({
    description: 'true if the user has seen the episode',
    example: true,
  })
  @Prop()
  hasSeen?: boolean;

  @ApiProperty({
    description: 'The number of the last episode seen',
    example: 2,
  })
  @Prop()
  lasEpisodeSeen?: number;

  @ApiProperty({
    description: 'media type',
    example: 'serie',
  })
  @Prop()
  type: string;
}

export const SerieSchema = SchemaFactory.createForClass(Serie);

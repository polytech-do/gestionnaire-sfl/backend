import { Controller, Get, UseGuards, Req } from '@nestjs/common';
import { ApiNotFoundResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { JwtAuthGuard } from 'src/app/auth/jwt.guard';
import { JwtRequest } from 'src/app/auth/jwt/jwt-request.type';
import { Endpoint } from '../endpoint';
import { MediaService } from './media.service';
import { Media } from './schema/media.schema';

@ApiTags(Endpoint.MEDIA)
@ApiUnauthorizedResponse({ description: 'You need to be conected to access to this ressource' })
@ApiNotFoundResponse({ description: 'The ressource was not found' })
@UseGuards(JwtAuthGuard)
@Controller(Endpoint.MEDIA)
export class MediaController {
  constructor(private readonly mediaService: MediaService) {}

  @Get()
  findAll(@Req() req: JwtRequest): Observable<Media[]> {
    return this.mediaService.findAll(req.user);
  }
}

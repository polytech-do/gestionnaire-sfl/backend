import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { Genre } from 'src/app/genre/schema/genre.schema';
import { Author } from 'src/app/author/schema/author.schema';
import { User } from 'src/app/user/schema/user.schema';

export type FilmDocument = Film & Document;

@Schema()
export class Film {
  @ApiProperty({
    description: 'id of the film',
    example: '5ff35a19056faa5b90bdae17',
  })
  _id: string;

  @ApiProperty({
    description: 'title of the film',
    example: 'Monty Python, sacré Graal',
  })
  @Prop()
  title: string;

  @ApiProperty({
    description: 'description of the film',
    example:
      'Le roi Arthur et les Chevaliers de la Table Ronde se lancent à la conquête du Graal, chevauchant de fantômatiques montures dans un bruitage de noix de coco cognées.',
  })
  @Prop()
  description?: string;

  @ApiProperty({
    description: 'url of the film image',
    example: 'https://fr.web.img6.acsta.net/c_310_420/medias/nmedia/00/02/43/01/graal.jpg',
  })
  @Prop()
  image?: string;

  @ApiProperty({
    description: 'rate of the film',
    example: 5,
    maximum: 5,
  })
  @Prop({ max: 5 })
  rate?: number;

  @ApiProperty({
    description: 'list of genres of the film',
    type: [Genre],
  })
  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: Genre.name }] })
  genres?: Genre[];

  @ApiProperty({
    description: 'authors of the film',
    type: [Author],
  })
  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: Author.name }] })
  authors?: Author[];

  @ApiProperty({
    description: 'owner of the film',
    type: User,
  })
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  owner: User;

  @ApiProperty({
    description: "if true the film is one of the user's favourites",
    example: true,
  })
  @Prop()
  love: boolean;

  @ApiProperty({
    description: 'if true the film is in the wish list of the user',
    example: false,
  })
  @Prop()
  wish: boolean;

  @ApiProperty({
    description: 'if true the user watch the film',
    example: true,
  })
  @Prop()
  inProgress: boolean;

  @ApiProperty({
    description: 'media type',
    example: 'film',
  })
  @Prop()
  type: string;
}

export const FilmSchema = SchemaFactory.createForClass(Film);

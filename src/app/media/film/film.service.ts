import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Author } from 'src/app/author/schema/author.schema';
import { MediaQuerryParams } from 'src/app/common/model/media-querry-params.type';
import { TypeEnum } from 'src/app/common/model/type-enum';
import { Genre } from 'src/app/genre/schema/genre.schema';
import { CreateUserDto } from 'src/app/user/dto/create-user.dto';
import { User } from 'src/app/user/schema/user.schema';
import { UserService } from 'src/app/user/user.service';
import { CreateFilmDto } from './dto/create-film.dto';
import { UpdateFilmDto } from './dto/update-film.dto';
import { FilmFilters } from './film.filters';
import { Film, FilmDocument } from './shema/film.shema';

@Injectable()
export class FilmService {
  constructor(
    @InjectModel(Film.name) private filmModel: Model<FilmDocument>,
    private filmFilters: FilmFilters,
    private userService: UserService
  ) {}

  create(createFilmDto: CreateFilmDto, me: CreateUserDto): Observable<Film> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          const createFilm: CreateFilmDto = { ...createFilmDto, owner: user };
          const createdFilm: FilmDocument = new this.filmModel(createFilm);
          return createdFilm.save();
        }
      })
    );
  }

  findAll(me: CreateUserDto, queries?: MediaQuerryParams): Observable<Film[]> {
    return this.filmFilters.buildFilters(queries, me).pipe(
      switchMap((filters: MediaQuerryParams) => {
        return this.filmModel
          .find(filters)
          .populate({ path: 'genres', model: Genre.name })
          .populate({ path: 'authors', model: Author.name })
          .populate({ path: 'owner', select: '-token', model: User.name })
          .exec();
      })
    );
  }

  findOne(id: string, me: CreateUserDto): Observable<Film> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return from(
            this.filmModel
              .findOne({ _id: id, owner: user })
              .populate({ path: 'genres', model: Genre.name })
              .populate({ path: 'authors', model: Author.name })
              .populate({ path: 'owner', select: '-token', model: User.name })
              .exec()
          ).pipe(
            tap((film: Film) => {
              if (!film) {
                throw new NotFoundException();
              }
            })
          );
        }
      })
    );
  }

  update(id: string, updateFilmDto: UpdateFilmDto, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.filmModel.updateOne({ _id: id, owner: user }, updateFilmDto, { new: true });
        }
      }),
      map(() => {
        return;
      })
    );
  }

  remove(id: string, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.filmModel.deleteOne({ _id: id, owner: user });
        }
      }),
      map(() => {
        return;
      })
    );
  }

  count(me: CreateUserDto): Observable<number> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.filmModel.countDocuments({ type: TypeEnum.film, owner: user });
        }
      })
    );
  }
}

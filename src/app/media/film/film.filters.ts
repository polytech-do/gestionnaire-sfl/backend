import { Injectable, NotFoundException } from '@nestjs/common';
import { MediaQuerryParams } from 'src/app/common/model/media-querry-params.type';
import { has } from 'lodash';
import { TypeEnum } from 'src/app/common/model/type-enum';
import { UserService } from 'src/app/user/user.service';
import { CreateUserDto } from 'src/app/user/dto/create-user.dto';
import { Observable } from 'rxjs';
import { User } from 'src/app/user/schema/user.schema';
import { map } from 'rxjs/operators';

@Injectable()
export class FilmFilters {
  constructor(private userService: UserService) {}

  buildFilters(querry: MediaQuerryParams, me: CreateUserDto): Observable<MediaQuerryParams & { type: TypeEnum.film }> {
    return this.userService.findOneByEmail(me.email).pipe(
      map((user: User) => {
        if (!user) {
          throw new NotFoundException();
        }
        const filter: MediaQuerryParams & { type: TypeEnum.film } = { type: TypeEnum.film, owner: user };
        if (has(querry, 'inProgress')) {
          filter.inProgress = querry.inProgress;
        }
        if (has(querry, 'title')) {
          filter.title = querry.title;
        }
        if (has(querry, 'wish')) {
          filter.wish = querry.wish;
        }
        if (has(querry, 'love')) {
          filter.love = querry.love;
        }
        return filter;
      })
    );
  }
}

import { Module } from '@nestjs/common';
import { FilmService } from './film.service';
import { FilmController } from './film.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Film, FilmSchema } from './shema/film.shema';
import { FilmFilters } from './film.filters';
import { UserModule } from 'src/app/user/user.module';

@Module({
  imports: [UserModule, MongooseModule.forFeature([{ name: Film.name, schema: FilmSchema, collection: 'media' }])],
  controllers: [FilmController],
  providers: [FilmService, FilmFilters],
})
export class FilmModule {}

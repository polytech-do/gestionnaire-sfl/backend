import { ApiProperty } from '@nestjs/swagger';
import { Author } from 'src/app/author/schema/author.schema';
import { Genre } from 'src/app/genre/schema/genre.schema';
import { User } from 'src/app/user/schema/user.schema';

export class CreateFilmDto {
  @ApiProperty({
    description: 'title of the film',
    example: 'Monty Python, sacré Graal',
  })
  title: string;

  @ApiProperty({
    description: 'description of the film',
    example:
      'Le roi Arthur et les Chevaliers de la Table Ronde se lancent à la conquête du Graal, chevauchant de fantômatiques montures dans un bruitage de noix de coco cognées.',
  })
  description?: string;

  @ApiProperty({
    description: 'url of the film image',
    example: 'https://fr.web.img6.acsta.net/c_310_420/medias/nmedia/00/02/43/01/graal.jpg',
  })
  image?: string;

  @ApiProperty({
    description: 'rate of the film',
    example: 5,
  })
  rate?: number;

  @ApiProperty({
    description: 'list of genres of the film',
    type: [Genre],
  })
  genres?: Genre[];

  @ApiProperty({
    description: 'authors of the film',
    type: [Author],
  })
  authors?: Author[];

  @ApiProperty({
    description: 'owner of the film',
    type: User,
  })
  owner?: User;

  @ApiProperty({
    description: "if true the film is one of the user's favourites",
    example: true,
  })
  love: boolean;

  @ApiProperty({
    description: 'if true the film is in the wish list of the user',
    example: false,
  })
  wish: boolean;

  @ApiProperty({
    description: 'if true the user watch the film',
    example: true,
  })
  inProgress: boolean;

  @ApiProperty({
    description: 'media type',
    example: 'film',
  })
  type: string;
}

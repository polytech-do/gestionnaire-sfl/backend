import { Controller, Get, Post, Body, Put, Param, Delete, Query, UseGuards, Req } from '@nestjs/common';
import { FilmService } from './film.service';
import { CreateFilmDto } from './dto/create-film.dto';
import { UpdateFilmDto } from './dto/update-film.dto';
import { Endpoint } from '../../endpoint';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiParam,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { Film } from './shema/film.shema';
import { MediaQuerryParams } from 'src/app/common/model/media-querry-params.type';
import { JwtAuthGuard } from 'src/app/auth/jwt.guard';
import { JwtRequest } from 'src/app/auth/jwt/jwt-request.type';
import { IdValidationPipe } from 'src/app/common/pipes/id-validation.pipe';

@ApiTags(Endpoint.FILM)
@ApiUnauthorizedResponse({ description: 'You need to be conected to access to this ressource' })
@ApiNotFoundResponse({ description: 'The ressource was not found' })
@UseGuards(JwtAuthGuard)
@Controller(Endpoint.FILM)
export class FilmController {
  constructor(private readonly filmService: FilmService) {}

  @ApiBody({ type: CreateFilmDto })
  @ApiCreatedResponse({ description: 'The film has been successfully created', type: Film })
  @Post()
  create(@Req() req: JwtRequest, @Body() createFilmDto: CreateFilmDto): Observable<Film> {
    return this.filmService.create(createFilmDto, req.user);
  }

  @ApiOkResponse({ description: 'All films has been successfully returned', type: [Film] })
  @ApiQuery({ type: MediaQuerryParams })
  @Get()
  findAll(@Req() req: JwtRequest, @Query() queries: MediaQuerryParams): Observable<Film[]> {
    return this.filmService.findAll(req.user, queries);
  }

  @ApiOkResponse({ description: 'The count of film has been successfully returned' })
  @Get('count')
  count(@Req() req: JwtRequest): Observable<number> {
    return this.filmService.count(req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the film', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The film has been successfully returned', type: Film })
  @Get(':id')
  findOne(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<Film> {
    return this.filmService.findOne(id, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the film', example: '5ff35a19056faa5b90bdae17' })
  @ApiBody({ type: UpdateFilmDto })
  @ApiOkResponse({ description: 'The film has been successfully updated' })
  @Put(':id')
  update(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string, @Body() updateFilmDto: UpdateFilmDto): Observable<void> {
    return this.filmService.update(id, updateFilmDto, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the film', example: '5ff35a19056faa5b90bdae17' })
  @Delete(':id')
  remove(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<void> {
    return this.filmService.remove(id, req.user);
  }
}

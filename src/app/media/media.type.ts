import { Genre } from '../genre/schema/genre.schema';
import { Book } from './book/shema/book.shema';
import { Serie } from './serie/schema/serie.schema';

export type Media = Serie | Genre | Book;

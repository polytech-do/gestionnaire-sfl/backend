import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BookModule } from './book/book.module';
import { FilmModule } from './film/film.module';
import { MediaController } from './media.controller';
import { MediaService } from './media.service';
import { Media, MediaSchema } from './schema/media.schema';
import { SerieModule } from './serie/serie.module';

@Module({
  imports: [
    BookModule,
    FilmModule,
    SerieModule,
    MongooseModule.forFeature([{ name: Media.name, schema: MediaSchema, collection: 'media' }]),
  ],
  exports: [BookModule, FilmModule, SerieModule],
  controllers: [MediaController],
  providers: [MediaService],
})
export class MediaModule {}

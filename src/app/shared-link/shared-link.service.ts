import { forwardRef, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { forkJoin, from, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { SharedListService } from '../shared-list/shared-list.service';
import { SharedList } from '../shared-list/shema/shared-list.shema';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { User } from '../user/schema/user.schema';
import { UserService } from '../user/user.service';
import { CreateSharedLinkDto } from './dto/create-shared-link.dto';
import { UpdateSharedLinkDto } from './dto/update-shared-link.dto';
import { SharedLink, SharedLinkDocument } from './shema/shared-link.shema';

@Injectable()
export class SharedLinkService {
  constructor(
    @InjectModel(SharedLink.name) private sharedLinkModel: Model<SharedLinkDocument>,
    @Inject(forwardRef(() => UserService))
    private userService: UserService,
    private sharedListService: SharedListService
  ) {}

  create(createSharedLinkDto: CreateSharedLinkDto, me: CreateUserDto): Observable<SharedLink> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.sharedListService
            .create({
              label: createSharedLinkDto.sharedList.label,
              author: user,
            })
            .pipe(
              switchMap((sharedList: SharedList) => {
                createSharedLinkDto.sharedList._id = sharedList._id;
                const createSharedLink: CreateSharedLinkDto = { ...createSharedLinkDto, owner: user };
                const createdSharedLink: SharedLinkDocument = new this.sharedLinkModel(createSharedLink);
                return createdSharedLink.save();
              })
            );
        }
      })
    );
  }

  findAll(me: CreateUserDto): Observable<SharedLink[]> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.sharedLinkModel
            .find({ owner: user })
            .populate({ path: 'owner', select: '-token', model: User.name })
            .populate({ path: 'sharedList', select: '-series -films -books -comments', model: SharedList.name })
            .exec();
        }
      })
    );
  }

  findOne(id: string, me: CreateUserDto): Observable<SharedLink> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return from(
            this.sharedLinkModel
              .findOne({ _id: id, owner: user })
              .populate({ path: 'owner', select: '-token', model: User.name })
              .populate({ path: 'sharedList', select: '-series -films -books -comments', model: SharedList.name })
              .exec()
          ).pipe(
            tap((sharedLink: SharedLink) => {
              if (!sharedLink) {
                throw new NotFoundException();
              }
            })
          );
        }
      })
    );
  }

  update(id: string, updateSharedLinkDto: UpdateSharedLinkDto, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.sharedLinkModel.updateOne({ _id: id, owner: user }, updateSharedLinkDto, { new: true });
        }
      }),
      map(() => {
        return;
      })
    );
  }

  remove(id: string, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.findOne(id, me).pipe(
            switchMap((sharedLink: SharedLink) =>
              forkJoin([this.sharedListService.remove(sharedLink.sharedList._id), this.sharedLinkModel.deleteOne({ _id: id, owner: user })])
            )
          );
        }
      }),
      map(() => {
        return;
      })
    );
  }

  removeByOwner(owner: User): Observable<void> {
    return this.findAll(owner).pipe(
      switchMap((deletedSharedLink: SharedLink[]) => {
        return forkJoin([
          ...deletedSharedLink.map((s: SharedLink) => this.sharedListService.remove(s.sharedList._id)),
          this.sharedLinkModel.deleteMany({ owner: owner }),
        ]);
      }),
      map(() => {
        return;
      })
    );
  }
}

import { ApiProperty } from '@nestjs/swagger';
import { SharedList } from 'src/app/shared-list/shema/shared-list.shema';
import { User } from 'src/app/user/schema/user.schema';

export class CreateSharedLinkDto {
  @ApiProperty({
    description: 'owner of the link',
    type: User,
  })
  owner?: User;

  @ApiProperty({
    description: 'shared lisk assosied of with the link',
    type: SharedList,
  })
  sharedList: SharedList;
}

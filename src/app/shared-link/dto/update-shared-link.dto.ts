import { PartialType } from '@nestjs/swagger';
import { CreateSharedLinkDto } from './create-shared-link.dto';

export class UpdateSharedLinkDto extends PartialType(CreateSharedLinkDto) {}

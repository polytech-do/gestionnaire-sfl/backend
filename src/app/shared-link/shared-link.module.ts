import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from '../auth/auth.module';
import { SharedListModule } from '../shared-list/shared-list.module';
import { UserModule } from '../user/user.module';
import { SharedLinkController } from './shared-link.controller';
import { SharedLinkService } from './shared-link.service';
import { SharedLink, SharedLinkSchema } from './shema/shared-link.shema';

@Module({
  imports: [
    forwardRef(() => UserModule),
    forwardRef(() => SharedListModule),
    forwardRef(() => AuthModule),
    MongooseModule.forFeature([{ name: SharedLink.name, schema: SharedLinkSchema }]),
  ],
  controllers: [SharedLinkController],
  providers: [SharedLinkService],
  exports: [SharedLinkService],
})
export class SharedLinkModule {}

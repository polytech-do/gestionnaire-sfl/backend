import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { User } from 'src/app/user/schema/user.schema';
import { SharedList } from 'src/app/shared-list/shema/shared-list.shema';

export type SharedLinkDocument = SharedLink & Document;

@Schema()
export class SharedLink {
  @ApiProperty({
    description: 'id of the sharedList',
    example: '5ff35a19056faa5b90bdae17',
  })
  _id: string;

  @ApiProperty({
    description: 'owner of the link',
    type: User,
  })
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  owner: User;

  @ApiProperty({
    description: 'shared lisk assosied of with the link',
    type: SharedList,
  })
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: SharedList.name })
  sharedList: SharedList;
}

export const SharedLinkSchema = SchemaFactory.createForClass(SharedLink);

import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Req } from '@nestjs/common';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { JwtAuthGuard } from '../auth/jwt.guard';
import { JwtRequest } from '../auth/jwt/jwt-request.type';
import { IdValidationPipe } from '../common/pipes/id-validation.pipe';
import { Endpoint } from '../endpoint';
import { CreateSharedLinkDto } from './dto/create-shared-link.dto';
import { UpdateSharedLinkDto } from './dto/update-shared-link.dto';
import { SharedLinkService } from './shared-link.service';
import { SharedLink } from './shema/shared-link.shema';

@ApiTags(Endpoint.SHARED_LINK)
@ApiUnauthorizedResponse({ description: 'You need to be conected to access to this ressource' })
@ApiNotFoundResponse({ description: 'The ressource was not found' })
@UseGuards(JwtAuthGuard)
@Controller(Endpoint.SHARED_LINK)
export class SharedLinkController {
  constructor(private readonly sharedLinkService: SharedLinkService) {}

  @ApiBody({ type: CreateSharedLinkDto })
  @ApiCreatedResponse({ description: 'The sharedLink has been successfully created', type: SharedLink })
  @Post()
  create(@Req() req: JwtRequest, @Body() createSharedLinkDto: CreateSharedLinkDto): Observable<SharedLink> {
    return this.sharedLinkService.create(createSharedLinkDto, req.user);
  }

  @ApiOkResponse({ description: 'All sharedLink has been successfully returned', type: [SharedLink] })
  @Get()
  findAll(@Req() req: JwtRequest): Observable<SharedLink[]> {
    return this.sharedLinkService.findAll(req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the sharedLink', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The sharedLink has been successfully returned', type: SharedLink })
  @Get(':id')
  findOne(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<SharedLink> {
    return this.sharedLinkService.findOne(id, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the sharedLink', example: '5ff35a19056faa5b90bdae17' })
  @ApiBody({ type: UpdateSharedLinkDto })
  @ApiOkResponse({ description: 'The sharedLink has been successfully updated' })
  @Put(':id')
  update(
    @Req() req: JwtRequest,
    @Param('id', IdValidationPipe) id: string,
    @Body() updateSharedLinkDto: UpdateSharedLinkDto
  ): Observable<void> {
    return this.sharedLinkService.update(id, updateSharedLinkDto, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the sharedLink', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The sharedLink has been successfully deleted' })
  @Delete(':id')
  remove(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<void> {
    return this.sharedLinkService.remove(id, req.user);
  }
}

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { User } from 'src/app/user/schema/user.schema';
import * as mongoose from 'mongoose';

export type GenreDocument = Genre & Document;

@Schema()
export class Genre {
  @ApiProperty({
    description: 'id of the genre',
    example: '5ff35a19056faa5b90bdae17',
  })
  _id: string;

  @ApiProperty({
    description: 'label of the genre',
    example: 'Action',
  })
  @Prop()
  label: string;

  @ApiProperty({
    description: 'owner of the genre',
    type: User,
  })
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  owner: User;
}

export const GenreSchema = SchemaFactory.createForClass(Genre);

import { ApiProperty } from '@nestjs/swagger';
import { User } from 'src/app/user/schema/user.schema';

export class CreateGenreDto {
  @ApiProperty({
    description: 'label of the genre',
    example: 'Action',
  })
  label: string;

  @ApiProperty({
    description: 'owner of the genre',
    type: User,
  })
  owner?: User;
}

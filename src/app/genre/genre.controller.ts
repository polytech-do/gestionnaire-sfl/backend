import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Req } from '@nestjs/common';
import { GenreService } from './genre.service';
import { CreateGenreDto } from './dto/create-genre.dto';
import { UpdateGenreDto } from './dto/update-genre.dto';
import { Endpoint } from '../endpoint';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { Genre } from './schema/genre.schema';
import { JwtAuthGuard } from '../auth/jwt.guard';
import { JwtRequest } from '../auth/jwt/jwt-request.type';
import { IdValidationPipe } from '../common/pipes/id-validation.pipe';

@ApiTags(Endpoint.GENRE)
@ApiUnauthorizedResponse({ description: 'You need to be conected to access to this ressource' })
@ApiNotFoundResponse({ description: 'The ressource was not found' })
@UseGuards(JwtAuthGuard)
@Controller(Endpoint.GENRE)
export class GenreController {
  constructor(private readonly genreService: GenreService) {}

  @ApiBody({ type: CreateGenreDto })
  @ApiCreatedResponse({ description: 'The genre has been successfully created', type: Genre })
  @Post()
  create(@Req() req: JwtRequest, @Body() createGenreDto: CreateGenreDto): Observable<Genre> {
    return this.genreService.create(createGenreDto, req.user);
  }

  @ApiOkResponse({ description: 'All genres has been successfully returned', type: [Genre] })
  @Get()
  findAll(@Req() req: JwtRequest): Observable<Genre[]> {
    return this.genreService.findAll(req.user);
  }

  @ApiOkResponse({ description: 'The count of genre has been successfully returned' })
  @Get('count')
  count(@Req() req: JwtRequest): Observable<number> {
    return this.genreService.count(req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the genre', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The genre has been successfully returned', type: Genre })
  @Get(':id')
  findOne(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<Genre> {
    return this.genreService.findOne(id, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the genre', example: '5ff35a19056faa5b90bdae17' })
  @ApiBody({ type: UpdateGenreDto })
  @ApiOkResponse({ description: 'The genre has been successfully updated' })
  @Put(':id')
  update(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string, @Body() updateGenreDto: UpdateGenreDto): Observable<void> {
    return this.genreService.update(id, updateGenreDto, req.user);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the genre', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The genre has been successfully deleted' })
  @Delete(':id')
  remove(@Req() req: JwtRequest, @Param('id', IdValidationPipe) id: string): Observable<void> {
    return this.genreService.remove(id, req.user);
  }
}

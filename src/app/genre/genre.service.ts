import { forwardRef, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Media, MediaDocument } from '../media/schema/media.schema';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { User } from '../user/schema/user.schema';
import { UserService } from '../user/user.service';
import { CreateGenreDto } from './dto/create-genre.dto';
import { UpdateGenreDto } from './dto/update-genre.dto';
import { Genre, GenreDocument } from './schema/genre.schema';

@Injectable()
export class GenreService {
  constructor(
    @InjectModel(Genre.name) private genreModel: Model<GenreDocument>,
    @InjectModel(Media.name) private mediaModel: Model<MediaDocument>,
    @Inject(forwardRef(() => UserService))
    private userService: UserService
  ) {}

  create(createGenreDto: CreateGenreDto, me: CreateUserDto): Observable<Genre> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          const createGenre: CreateGenreDto = { ...createGenreDto, owner: user };
          const createdGenre: GenreDocument = new this.genreModel(createGenre);
          return createdGenre.save();
        }
      })
    );
  }

  findAll(me: CreateUserDto): Observable<Genre[]> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.genreModel.find({ owner: user }).populate({ path: 'owner', select: '-token', model: User.name }).exec();
        }
      })
    );
  }

  findOne(id: string, me: CreateUserDto): Observable<Genre> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return from(
            this.genreModel.findOne({ _id: id, owner: user }).populate({ path: 'owner', select: '-token', model: User.name }).exec()
          ).pipe(
            tap((genre: Genre) => {
              if (!genre) {
                throw new NotFoundException();
              }
            })
          );
        }
      })
    );
  }

  update(id: string, updateGenreDto: UpdateGenreDto, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.genreModel.updateOne({ _id: id, owner: user }, updateGenreDto, { new: true });
        }
      }),
      map(() => {
        return;
      })
    );
  }

  remove(id: string, me: CreateUserDto): Observable<void> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return from(this.findOne(id, me)).pipe(
            switchMap((genre: Genre) => {
              return from(this.mediaModel.updateMany({ genres: genre }, { $pullAll: { genres: [genre] } })).pipe(
                switchMap(() => this.genreModel.deleteOne({ _id: id, owner: user }))
              );
            })
          );
        }
      }),
      map(() => {
        return;
      })
    );
  }

  removeByOwner(owner: User): Observable<void> {
    return from(this.genreModel.deleteMany({ owner: owner })).pipe(
      map(() => {
        return;
      })
    );
  }

  count(me: CreateUserDto): Observable<number> {
    return this.userService.findOneByEmail(me.email).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new NotFoundException();
        } else {
          return this.genreModel.countDocuments({ owner: user });
        }
      })
    );
  }
}

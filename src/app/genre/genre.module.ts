import { forwardRef, Module } from '@nestjs/common';
import { GenreService } from './genre.service';
import { GenreController } from './genre.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Genre, GenreSchema } from './schema/genre.schema';
import { AuthModule } from '../auth/auth.module';
import { UserModule } from '../user/user.module';
import { Media, MediaSchema } from '../media/schema/media.schema';

@Module({
  imports: [
    forwardRef(() => UserModule),
    AuthModule,
    MongooseModule.forFeature([{ name: Genre.name, schema: GenreSchema }]),
    MongooseModule.forFeature([{ name: Media.name, schema: MediaSchema, collection: 'media' }]),
  ],
  controllers: [GenreController],
  providers: [GenreService],
  exports: [GenreService],
})
export class GenreModule {}

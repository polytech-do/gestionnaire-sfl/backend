import { PartialType } from '@nestjs/swagger';
import { CreateSharedListDto } from './create-shared-list.dto';

export class UpdateSharedListDto extends PartialType(CreateSharedListDto) {}

import { ApiProperty } from '@nestjs/swagger';
import { User } from 'src/app/user/schema/user.schema';

export class CreateCommentDto {
  @ApiProperty({
    description: 'comment',
    example: 'Super ! Bravo !',
    type: String,
  })
  label: string;

  @ApiProperty({
    description: 'author of the comment',
    type: User,
  })
  author: User;
}

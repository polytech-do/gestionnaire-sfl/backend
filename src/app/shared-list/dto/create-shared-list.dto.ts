import { ApiProperty } from '@nestjs/swagger';
import { Book } from 'src/app/media/book/shema/book.shema';
import { Film } from 'src/app/media/film/shema/film.shema';
import { Serie } from 'src/app/media/serie/schema/serie.schema';
import { User } from 'src/app/user/schema/user.schema';

export class CreateSharedListDto {
  @ApiProperty({
    description: 'label of the genre',
    example: 'Action',
  })
  label: string;

  @ApiProperty({
    description: 'author of the list',
    type: User,
  })
  author: User;

  @ApiProperty({
    description: 'series linked to the sharedList',
  })
  series?: Serie[];

  @ApiProperty({
    description: 'films linked to the sharedList',
  })
  films?: Film[];

  @ApiProperty({
    description: 'books linked to the sharedList',
  })
  books?: Book[];
}

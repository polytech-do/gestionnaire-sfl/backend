import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { Book } from 'src/app/media/book/shema/book.shema';
import { Film } from 'src/app/media/film/shema/film.shema';
import { Serie } from 'src/app/media/serie/schema/serie.schema';
import * as mongoose from 'mongoose';
import { Comment } from '../type/comment.type';
import { User } from 'src/app/user/schema/user.schema';

export type SharedListDocument = SharedList & Document;

@Schema()
export class SharedList {
  @ApiProperty({
    description: 'id of the sharedList',
    example: '5ff35a19056faa5b90bdae17',
  })
  _id: string;

  @ApiProperty({
    description: 'label of the sharedList',
    example: 'Mes films favoris',
  })
  @Prop()
  label: string;

  @ApiProperty({
    description: 'author of the list',
    type: User,
  })
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  author: User;

  @ApiProperty({
    description: 'series linked to the sharedList',
    type: [Serie],
  })
  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: Serie.name }] })
  series?: Serie[];

  @ApiProperty({
    description: 'films linked to the sharedList',
    type: [Film],
  })
  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: Film.name }] })
  films?: Film[];

  @ApiProperty({
    description: 'books linked to the sharedList',
    type: [Book],
  })
  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: Book.name }] })
  books?: Book[];

  @ApiProperty({
    description: 'Comment of to the sharedList',
    type: [Comment],
  })
  @Prop({ type: Comment })
  comments?: Comment[];
}

export const SharedListSchema = SchemaFactory.createForClass(SharedList);

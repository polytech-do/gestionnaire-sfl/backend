import { forwardRef, Module } from '@nestjs/common';
import { SharedListService } from './shared-list.service';
import { SharedListController } from './shared-list.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { SharedList, SharedListSchema } from './shema/shared-list.shema';
import { UserModule } from '../user/user.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    forwardRef(() => UserModule),
    forwardRef(() => AuthModule),
    MongooseModule.forFeature([{ name: SharedList.name, schema: SharedListSchema }]),
  ],
  controllers: [SharedListController],
  providers: [SharedListService],
  exports: [SharedListService],
})
export class SharedListModule {}

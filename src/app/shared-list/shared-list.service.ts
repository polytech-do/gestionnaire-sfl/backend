import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Book } from '../media/book/shema/book.shema';
import { Film } from '../media/film/shema/film.shema';
import { User } from '../user/schema/user.schema';
import { CreateSharedListDto } from './dto/create-shared-list.dto';
import { UpdateSharedListDto } from './dto/update-shared-list.dto';
import { SharedList, SharedListDocument } from './shema/shared-list.shema';

@Injectable()
export class SharedListService {
  constructor(@InjectModel(SharedList.name) private SharedListModel: Model<SharedListDocument>) {}

  create(createSharedListDto: CreateSharedListDto): Observable<SharedList> {
    const userUser: SharedListDocument = new this.SharedListModel(createSharedListDto);
    return from(userUser.save());
  }

  findAll(): Observable<SharedList[]> {
    return from(
      this.SharedListModel.find()
        .populate({ path: 'series', model: 'Serie' })
        .populate({ path: 'films', model: Film.name })
        .populate({ path: 'books', model: Book.name })
        .populate({ path: 'author', select: '-token', model: User.name })
        .exec()
    );
  }

  findOne(id: string): Observable<SharedList> {
    return from(
      this.SharedListModel.findById(id)
        .populate({ path: 'series', model: 'Serie' })
        .populate({ path: 'films', model: Film.name })
        .populate({ path: 'books', model: Book.name })
        .populate({ path: 'author', select: '-token', model: User.name })
        .exec()
    ).pipe(
      tap((sharedListDocument: SharedListDocument) => {
        if (!sharedListDocument) {
          throw new NotFoundException();
        }
      })
    );
  }

  update(id: string, updateSharedListDto: UpdateSharedListDto): Observable<void> {
    return from(this.SharedListModel.updateOne({ _id: id }, updateSharedListDto, { new: true })).pipe(
      map(() => {
        return;
      })
    );
  }

  remove(id: string): Observable<void> {
    return from(this.SharedListModel.deleteOne({ _id: id })).pipe(
      map(() => {
        return;
      })
    );
  }

  // createComment(createCommentDto: CreateCommentDto, me: CreateUserDto): Observable<Comment> | any{
  //   return this.userService.findOneByEmail(me.email).pipe(switchMap((user: User) => {
  //     if (!user) {
  //       throw new NotFoundException();
  //     } else {
  //       return this.SharedListModel.updateOne()
  //     }
  //   }));
  // }

  // deleteComment(createCommentDto: CreateCommentDto, me: CreateUserDto): Observable<Comment> {
  //   return;
  // }
}

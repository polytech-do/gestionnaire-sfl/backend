import { Body, Controller, Get, Param, Put, UseGuards } from '@nestjs/common';
import { SharedListService } from './shared-list.service';
import { ApiBody, ApiNotFoundResponse, ApiOkResponse, ApiParam, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { Endpoint } from '../endpoint';
import { SharedList } from './shema/shared-list.shema';
import { Observable } from 'rxjs';
import { UpdateSharedListDto } from './dto/update-shared-list.dto';
import { JwtAuthGuard } from '../auth/jwt.guard';
import { IdValidationPipe } from '../common/pipes/id-validation.pipe';

@ApiTags(Endpoint.SHARED_LIST)
@ApiUnauthorizedResponse({ description: 'You need to be conected to access to this ressource' })
@ApiNotFoundResponse({ description: 'The ressource was not found' })
@Controller(Endpoint.SHARED_LIST)
export class SharedListController {
  constructor(private readonly sharedListService: SharedListService) {}

  // @ApiBody({ type: CreateSharedListDto })
  // @ApiCreatedResponse({ description: 'The sharedList has been successfully created', type: SharedList })
  // @Post()
  // create(@Body() createSharedListDto: CreateSharedListDto): Observable<SharedList> {
  //   return this.sharedListService.create(createSharedListDto);
  // }

  // @ApiOkResponse({ description: 'All sharedList has been successfully returned', type: [SharedList] })
  // @Get()
  // findAll(): Observable<SharedList[]> {
  //   return this.sharedListService.findAll();
  // }

  @ApiParam({ name: 'id', type: String, description: 'id of the sharedList', example: '5ff35a19056faa5b90bdae17' })
  @ApiOkResponse({ description: 'The sharedList has been successfully returned', type: SharedList })
  @Get(':id')
  findOne(@Param('id', IdValidationPipe) id: string): Observable<SharedList> {
    return this.sharedListService.findOne(id);
  }

  @ApiParam({ name: 'id', type: String, description: 'id of the sharedList', example: '5ff35a19056faa5b90bdae17' })
  @ApiBody({ type: UpdateSharedListDto })
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ description: 'The sharedList has been successfully updated' })
  @Put(':id')
  update(@Param('id', IdValidationPipe) id: string, @Body() updateSharedListDto: UpdateSharedListDto): Observable<void> {
    return this.sharedListService.update(id, updateSharedListDto);
  }

  // @ApiParam({ name: 'id', type: String, description: 'id of the sharedList', example: '5ff35a19056faa5b90bdae17' })
  // @ApiOkResponse({ description: 'The sharedList has been successfully deleted' })
  // @Delete(':id')
  // remove(@Param('id', IdValidationPipe) id: string): Observable<void> {
  //   return this.sharedListService.remove(id);
  // }
}

import { Prop, Schema } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { User } from 'src/app/user/schema/user.schema';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

@Schema()
export class Comment extends Document {
  @ApiProperty({
    description: 'id of the comment',
    example: '5ff35a19056faa5b90bdae17',
    type: String,
  })
  _id: string;

  @ApiProperty({
    description: 'comment text',
    example: 'Super ! Bravo !',
    type: String,
  })
  @Prop()
  text: string;

  @ApiProperty({
    description: 'author of the comment',
    type: User,
  })
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  author: User;
}

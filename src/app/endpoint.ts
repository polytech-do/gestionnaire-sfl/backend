export enum Endpoint {
  SERIE = 'serie',
  FILM = 'film',
  BOOK = 'book',
  GENRE = 'genre',
  AUTHOR = 'author',
  USER = 'user',
  SHARED_LIST = 'shared-list',
  SHARED_LINK = 'shared-link',
  COMMENT = 'comment',
  OAUTH2 = 'oauth2',
  MEDIA = 'media'
}

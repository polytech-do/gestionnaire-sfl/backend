import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { blue, bold, bgBlue, white } from 'colors/safe';
import { Request } from 'express';
import { isEmpty } from 'lodash';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class HttpLoggerInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const req: Request = context.switchToHttp().getRequest();
    let log = `${bgBlue(white(bold(`[HTTP ${req.method}]`)))} ${req.originalUrl}`;
    if (!isEmpty(req.params)) {
      log += ` - ${blue('Params')} : ${JSON.stringify(req.params)}`;
    }
    if (!isEmpty(req.query)) {
      log += ` - ${blue('Query')} : ${JSON.stringify(req.query)}`;
    }
    if (!isEmpty(req.body)) {
      log += ` - ${blue('Body')} : ${JSON.stringify(req.body)}`;
    }
    return next.handle().pipe(
      tap(() => {
        console.log(log);
      })
    );
  }
}

import { User } from 'src/app/user/schema/user.schema';

export class MediaQuerryParams {
  inProgress?: boolean;
  title?: string;
  wish?: boolean;
  love?: boolean;
  owner?: User;
}

import { Environment } from './environment';

export const environment: Environment = {
  urls: {
    front: 'http://localhost:4200',
    back: 'http://localhost:3000',
  },
  auth: {
    google: {
      callbackUrl: 'http://localhost:3000/oauth2/callback',
    },
  },
};

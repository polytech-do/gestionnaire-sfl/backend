export interface Environment {
  urls: {
    front: string;
    back: string;
  };
  auth: {
    google: {
      callbackUrl: string;
    };
  };
}

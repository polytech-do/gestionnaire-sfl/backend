import { Environment } from './environment';

export const environment: Environment = {
  urls: {
    front: 'https://sfl.malo-polese.fr',
    back: 'https://sfl-api.herokuapp.com',
  },
  auth: {
    google: {
      callbackUrl: 'https://sfl-api.herokuapp.com/oauth2/callback',
    },
  },
};

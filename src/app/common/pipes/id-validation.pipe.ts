import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import * as mongoose from 'mongoose';

@Injectable()
export class IdValidationPipe implements PipeTransform {
  transform(id: string) {
    if (mongoose.Types.ObjectId.isValid(id)) {
      return id;
    } else {
      throw new BadRequestException();
    }
  }
}

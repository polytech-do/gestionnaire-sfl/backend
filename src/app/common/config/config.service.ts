import { Injectable } from '@nestjs/common';
import { DatabaseConfig } from 'src/database/database-config';
import { Environment } from '../environments/environment';
import { environment as localEnv } from '../environments/environment.local';
import { environment as prodEnv } from '../environments/environment.prod';
@Injectable()
export class ConfigService {
  getDefaultDbConfig(): string {
    const databaseInformation: DatabaseConfig = this.getDatabaseInformation();
    if (this.getEnvArgument('ENV') === 'dev') {
      return `mongodb://${databaseInformation.username}:${databaseInformation.password}@${databaseInformation.host}:${databaseInformation.port}/${databaseInformation.database}`;
    }
    return `mongodb+srv://${databaseInformation.username}:${databaseInformation.password}@${databaseInformation.host}/${databaseInformation.database}`;
  }

  private getDatabaseInformation(): DatabaseConfig {
    return {
      host: this.getEnvArgument('DB_ADDRESS'),
      port: +this.getEnvArgument('DB_PORT'),
      username: this.getEnvArgument('DB_USERNAME'),
      password: this.getEnvArgument('DB_PASSWORD'),
      database: this.getEnvArgument('DATABASE'),
    };
  }

  private getEnvArgument(arg: string): string {
    return process.env[arg];
  }

  public getCurrentEnvironment(): Environment {
    switch (process.env.ENV) {
      case 'dev':
        return localEnv;
      case 'prod':
        return prodEnv;
      default:
        return prodEnv;
    }
  }
}

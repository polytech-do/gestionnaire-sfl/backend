import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { User } from 'src/app/user/schema/user.schema';
import * as mongoose from 'mongoose';

export type CommentDocument = Comment & Document;

@Schema()
export class Comment {
  @ApiProperty({
    description: 'id of the comment',
    example: '5ff35a19056faa5b90bdae17',
  })
  _id: string;

  @ApiProperty({
    description: 'comment text',
    example: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  })
  @Prop()
  text: string;

  @ApiProperty({
    description: 'author of the comment',
  })
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  author?: User;
}

export const CommentSchema = SchemaFactory.createForClass(Comment);

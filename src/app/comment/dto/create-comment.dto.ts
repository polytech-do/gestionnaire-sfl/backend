import { ApiProperty } from '@nestjs/swagger';
import { User } from 'src/app/user/schema/user.schema';

export class CreateCommentDto {
  @ApiProperty({
    description: 'comment',
    example: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  })
  label: string;

  @ApiProperty({
    description: 'author of the comment',
  })
  author: User;
}

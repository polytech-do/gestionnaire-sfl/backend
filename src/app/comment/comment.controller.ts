import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Endpoint } from '../endpoint';
import { CommentService } from './comment.service';

@ApiTags(Endpoint.COMMENT)
@Controller(Endpoint.COMMENT)
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  // @ApiBody({ type: CreateCommentDto })
  // @ApiCreatedResponse({ description: 'The comment has been successfully created', type: Comment })
  // @Post()
  // create(@Body() createCommentDto: CreateCommentDto): Observable<Comment> {
  //   return this.commentService.create(createCommentDto);
  // }

  // @ApiOkResponse({ description: 'All comment has been successfully returned', type: [Comment] })
  // @Get()
  // findAll(): Observable<Comment[]> {
  //   return this.commentService.findAll();
  // }

  // @ApiParam({ name: 'id', type: String, description: 'id of the comment', example: '5ff35a19056faa5b90bdae17' })
  // @ApiOkResponse({ description: 'The comment has been successfully returned', type: Comment })
  // @Get(':id')
  // findOne(@Param('id', IdValidationPipe) id: string): Observable<Comment> {
  //   return this.commentService.findOne(id);
  // }

  // @ApiParam({ name: 'id', type: String, description: 'id of the comment', example: '5ff35a19056faa5b90bdae17' })
  // @ApiBody({ type: UpdateCommentDto })
  // @ApiOkResponse({ description: 'The Comment has been successfully updated' })
  // @Put(':id')
  // update(@Param('id', IdValidationPipe) id: string, @Body() updateCommentDto: UpdateCommentDto): Observable<void> {
  //   return this.commentService.update(id, updateCommentDto);
  // }

  // @ApiParam({ name: 'id', type: String, description: 'id of the Comment', example: '5ff35a19056faa5b90bdae17' })
  // @ApiOkResponse({ description: 'The comment has been successfully deleted' })
  // @Delete(':id')
  // remove(@Param('id', IdValidationPipe) id: string): Observable<void> {
  //   return this.commentService.remove(id);
  // }
}

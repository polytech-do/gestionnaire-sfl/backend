import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CreateCommentDto } from './dto/create-comment.dto';
import { CommentDocument, Comment } from './schema/comment.schema';

@Injectable()
export class CommentService {
  constructor(@InjectModel(Comment.name) private commentModel: Model<CommentDocument>) {}

  create(createCommentDto: CreateCommentDto): Observable<Comment> {
    const userUser: CommentDocument = new this.commentModel(createCommentDto);
    return from(userUser.save());
  }

  // findAll(): Observable<Comment[]> {
  //   return from(
  //     this.commentModel.find().populate('User').exec()
  //   )
  // }

  // findOne(id: string): Observable<Comment> {
  //   return from(
  //     this.commentModel.findById(id).populate('user').exec()
  //   )
  // }

  // update(id: string, updateCommentDto: UpdateCommentDto): Observable<void> {
  //   return from(
  //     this.commentModel.updateOne({ _id: id }, updateCommentDto, { new: true })
  //   ).pipe(map(() => {
  //     return;
  //   }));
  // }

  remove(id: string): Observable<void> {
    return from(this.commentModel.deleteOne({ _id: id })).pipe(
      map(() => {
        return;
      })
    );
  }
}

import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../user/schema/user.schema';
import { UserService } from '../user/user.service';
import { GooglePayload } from './google/google-profile-playload.type';

@Injectable()
export class AuthService {
  constructor(private userService: UserService, private jwtService: JwtService) {}

  manageGoogleUser(profile: GooglePayload): Observable<User> {
    return this.userService.createOrUpdateUser({
      displayName: profile.displayName,
      email: profile.emails[0].value,
      avatar: profile.photos[0].value,
      token: null,
    });
  }

  login(user: User): Observable<string> {
    const token = this.jwtService.sign({
      email: user.email,
      displayName: user.displayName,
      avatar: user.avatar,
    });
    return this.updateToken(user.email, token).pipe(map(() => token));
  }

  extractToken(authorization: string): string {
    return authorization && authorization.split(' ')[1].trim();
  }

  updateToken(userEmail: string, token: string): Observable<void> {
    return this.userService.updateToken(userEmail, token).pipe(
      map(() => {
        return;
      })
    );
  }
}

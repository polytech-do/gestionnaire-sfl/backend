import { Injectable, Query, Req, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from 'src/app/common/config/config.service';
import { User } from 'src/app/user/schema/user.schema';
import { UserService } from 'src/app/user/user.service';
import { AuthService } from '../auth.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(private authService: AuthService, private userService: UserService, configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromUrlQueryParameter('access_token'),
        ExtractJwt.fromAuthHeaderAsBearerToken(),
      ]),
      ignoreExpiration: false,
      secretOrKey: process.env['AUTH_JWT_SECRET'],
      passReqToCallback: true,
    });
  }

  async validate(@Req() req, @Query('access_token') user) {
    const bearerToken = this.authService.extractToken(req.header('Authorization'));
    let queryResult: User;
    if (user) {
      queryResult = await this.userService.findOneByEmail(user.email).toPromise();
    } else {
      queryResult = await this.userService.findOneByToken(bearerToken).toPromise();
    }
    if (queryResult) {
      return this.buildMe(queryResult);
    } else {
      throw new UnauthorizedException();
    }
  }

  private buildMe(payload: User): User {
    return {
      _id: payload._id,
      email: payload.email,
      displayName: payload.displayName,
      avatar: payload.avatar,
    };
  }
}

import { User } from 'src/app/user/schema/user.schema';

export type JwtRequest = Request & { user: User };

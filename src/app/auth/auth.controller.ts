import { Controller, Get, Req, Request, Res, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { ConfigService } from '../common/config/config.service';
import { Endpoint } from '../endpoint';
import { AuthService } from './auth.service';
import { GoogleOAuth2Guard } from './google-oauth.guard';

@ApiTags(Endpoint.OAUTH2)
@Controller(Endpoint.OAUTH2)
export class AuthController {
  constructor(private authService: AuthService, private configService: ConfigService) {}

  @UseGuards(GoogleOAuth2Guard)
  @Get('login/google')
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  login(@Request() _req) {}

  @UseGuards(GoogleOAuth2Guard)
  @Get('callback')
  callbackGoogle(@Req() req, @Res() res: Response) {
    this.authService.login(req.user).subscribe((token: string) => {
      return res.redirect(`${this.configService.getCurrentEnvironment().urls.front}/#/redirect?token=${token}`);
    });
  }
}

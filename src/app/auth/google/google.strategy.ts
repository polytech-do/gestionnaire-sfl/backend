import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-google-oauth20';
import { ConfigService } from 'src/app/common/config/config.service';
import { AuthService } from '../auth.service';
import { GooglePayload } from './google-profile-playload.type';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(private authService: AuthService, private configService: ConfigService) {
    super({
      clientID: process.env['AUTH_GOOGLE_KEY'],
      clientSecret: process.env['AUTH_GOOGLE_SECRET'],
      callbackURL: configService.getCurrentEnvironment().auth.google.callbackUrl,
      passReqToCallback: true,
      scope: ['profile', 'email'],
      accessType: 'online',
      prompt: 'consent',
    });
  }

  async validate(req: any, code: string, refreshToken: string, profile: GooglePayload): Promise<any> {
    return new Promise((resolve, reject) => {
      this.authService.manageGoogleUser(profile).subscribe(
        (user) => {
          resolve(user);
        },
        () => reject()
      );
    });
  }
}

import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { AppController } from './app.controller';
import { ConfigModule } from './app/common/config/config.module';
import { HttpLoggerInterceptor } from './app/common/interceptors/http-logger.interceptor';
import { GenreModule } from './app/genre/genre.module';
import { DatabaseModule } from './database/database.module';
import { AuthorModule } from './app/author/author.module';
import { UserModule } from './app/user/user.module';
import { SharedListModule } from './app/shared-list/shared-list.module';
import { CommentModule } from './app/comment/comment.module';
import { AuthModule } from './app/auth/auth.module';
import { SharedLinkModule } from './app/shared-link/shared-link.module';
import { ScheduleModule } from '@nestjs/schedule';
import { AppService } from './app.service';
import { MediaModule } from './app/media/media.module';

@Module({
  imports: [
    MediaModule,
    GenreModule,
    AuthorModule,
    UserModule,
    SharedLinkModule,
    SharedListModule,
    CommentModule,
    DatabaseModule,
    ConfigModule,
    AuthModule,
    ScheduleModule.forRoot(),
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: HttpLoggerInterceptor,
    },
    AppService,
  ],
})
export class AppModule {}

import { Controller, Delete, Get, Render, Req, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';
import { AuthService } from './app/auth/auth.service';
import { JwtAuthGuard } from './app/auth/jwt.guard';
import { JwtRequest } from './app/auth/jwt/jwt-request.type';
import { ConfigService } from './app/common/config/config.service';

@Controller()
export class AppController {
  constructor(private authService: AuthService, private configService: ConfigService) {}

  @Get()
  @Render('welcome')
  welcome() {
    return { message: 'Welcome to the sfl api', url: this.configService.getCurrentEnvironment().urls.back };
  }

  @UseGuards(JwtAuthGuard)
  @Get('me')
  me(@Req() req: JwtRequest) {
    return req.user;
  }

  @UseGuards(JwtAuthGuard)
  @Delete('me')
  deleteMe(@Req() req: JwtRequest, @Res() res: Response) {
    this.authService.updateToken(req.user.email, null).subscribe(() => {
      return res.sendStatus(204);
    });
  }
}

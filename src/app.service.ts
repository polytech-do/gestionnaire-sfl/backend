import { Injectable, Logger, OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common';
import { rainbow, red } from 'colors/safe';

@Injectable()
export class AppService implements OnApplicationShutdown, OnApplicationBootstrap {
  private readonly logger = new Logger(AppService.name);

  onApplicationBootstrap() {
    this.logger.getTimestamp();
    this.logger.log(rainbow(`SFL Backend open on port ${process.env.PORT || 3000}`));
  }
  onApplicationShutdown(signal: string) {
    console.log('\n');
    this.logger.getTimestamp();
    this.logger.log(`${red(`[${signal}]`)} SFL Backend Stop`);
  }
}
